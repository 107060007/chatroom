const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    devtool: "eval-source-map",
    entry: {
        login: "./public/login.js",
        chatroom: "./public/chatroom.js"
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js'
    },
    resolve: {
        // fallback: {
        //     "fs": false,
        //     "tls": false,
        //     "net": false,
        //     "path": false,
        //     "zlib": false,
        //     "http": false,
        //     "https": false,
        //     "stream": false,
        //     "crypto": false,
        //     "assert": false,
        //     "vm": false,
        //     "os": false,
        //     "constants": false,
        //     "child_process": false,
        //     "worker_threads": false,
        //     "crypto-browserify": require.resolve('crypto-browserify')
        // }
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader'
                ]
            },
            {
                test: /\.(scss|sass)$/,
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader'
                    },
                    {
                        loader: 'sass-loader'
                    }
                ]
            },
            {
                test: /\.svg$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            name: '[name].[ext]',
                        }
                    }
                ]
            },
            {
                test: /\.png$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            name: '[name].[ext]',
                        }
                    }
                ]
            }
        ]
    },
    mode: 'development',
    devServer: {
        hot: true,
        compress: true,
        host: 'localhost',
        port: 8080,
        open: true
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "public/index.html",
            filename: 'index.html',
            chunks: ["login"]
        }),
        new HtmlWebpackPlugin({
            template: "public/chatroom.html",
            filename: 'chatroom.html',
            chunks: ["chatroom"]
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        }),
        new MiniCssExtractPlugin(),
        new webpack.HotModuleReplacementPlugin()
    ]
};
