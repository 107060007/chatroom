# Software Studio 2021 Spring Midterm Project

## Topic
* Project Name : chatroom

## Basic Components
|      Component       | Score |  Y/N  |
| :------------------: | :---: | :---: |
| Membership Mechanism |  15%  |   Y   |
|    Firebase Page     |  5%   |   Y   |
|       Database       |  15%  |   Y   |
|         RWD          |  15%  |   Y   |
|  Topic Key Function  |  20%  |   Y   |

## Advanced Components
|      Component      | Score |  Y/N  |
| :-----------------: | :---: | :---: |
| Third-Party Sign In | 2.5%  |   Y   |
| Chrome Notification |  5%   |   Y   |
|  Use CSS Animation  | 2.5%  |   Y   |
|   Security Report   |  5%   |   Y   |

# 作品網址：(https://midterm-project-eed5c.web.app/)

## Website Detail Description
我實作了一個類似telegram的聊天室，基礎的對話功能與歷史紀錄瀏覽都有，還能上傳喜歡的大頭貼，也可以創建私人聊天室。


# Components Description : 
1. Membership Mechanism : 我實做了新增群組的功能，只有被加進群組的人可以看到聊天訊息，同時也能決定群組名稱。
2. RWD : 我實作的RWD，以保留聊天視窗為主，當畫面太小，左邊的朋友列表會縮小。
3. Third-Party Sign In : 我實作了google登入。
4. Use CSS Animation : 在聊天室窗內，我實作了蒲公英飛舞的特效，在邊框則有流動線條。
5. Security Report : 我使用<pre>來顯示message，這個tag會自動吃掉html tag。
6. Logout : 當然也有登出的功能，就在大頭貼的右側。

# Other Functions Description : 
1. 大頭貼 : 點擊右上角的人像圖案後，可以上傳自己的照片作為大頭貼。
2. 上傳圖片 : 可以點擊迴紋針圖案，就可以上傳圖片。

