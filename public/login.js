import("bootstrap")
import("bootstrap/dist/css/bootstrap.min.css")
import("@fortawesome/fontawesome-free")
import("./login.css")

import "./config.js"
import firebase from "firebase/app"

import "firebase/auth"
import "firebase/database"

function toggleSignUp(e) {
    e.preventDefault();
    $('#logreg-forms .form-signin').toggle(); // display:block or none
    $('#logreg-forms .form-signup').toggle(); // display:block or none
}

$(() => {
    $('#logreg-forms #btnSignUpNewAccount').click(toggleSignUp);
    $('#logreg-forms #cancelSignUp').click(toggleSignUp);
})

function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');
    var newEmail = document.getElementById('newEmail');
    var newPassword = document.getElementById('newPassword');

    btnLogin.addEventListener('click', function (event) {
        event.preventDefault();
        firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value).then(function () {
            document.location.href = "./chatroom.html";
        }).catch(function (error) {
            txtEmail.value = "";
            txtPassword.value = "";
            create_alert("error", "Account have not registered");
        });
    });

    btnGoogle.addEventListener('click', function (event) {
        event.preventDefault();
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function (result) {
            // var Ref = firebase.database().ref("register");
            // alert(result.user.email);
            // Ref.push({
            //     email: result.user.email,
            //     type: 0
            // }).then(() => {
            document.location.href = "chatroom.html";
            // });
        }).catch(function (error) {
            create_alert('error', error.message);
        });

    });

    btnSignUp.addEventListener('click', function (event) {
        event.preventDefault();
        firebase.auth().createUserWithEmailAndPassword(newEmail.value, newPassword.value).then(function (user) {
            $('#logreg-forms .form-signin').toggle(); // display:block or none
            $('#logreg-forms .form-signup').toggle(); // display:block or none
            newEmail.value = "";
            newPassword.value = "";
            create_alert("success", "Sign up succed");
        }).catch(function (error) {
            newEmail.value = "";
            newPassword.value = "";
            create_alert("error", "Sign up failed");
        });
    });
}

function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    var str_html = "";
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};