import("bootstrap")
import("bootstrap/dist/css/bootstrap.min.css")
import("./chatroom.css")
import("./chatroom.scss")

import "./config.js"
import firebase from "firebase/app"

import "firebase/auth"
import "firebase/database"
import "firebase/storage"

import './account_circle_white_48dp.svg';
import './groups_white_48dp.svg';
import './user-circle-solid.svg';
import './logout_white_36dp.svg';

var groupName = '';
var userEmail = '';
var userName = '';
var friendEmail = '';
var friendName = '';
var clickGroup = false;
var userRef = '';
var userGroupRef = '';
var userProfileRef = '';
var friendRef = '';
var groupRef = '';
var first_count_person = 0;
var second_count_person = 0;
var first_count_group = 0;
var second_count_group = 0;
var registerRef = firebase.database().ref("register");
var post_btn = document.getElementById('submit');
var post_txt = document.getElementById('input');
var addGroup = document.getElementById('groupName');
var addMember = document.getElementById('addMember');
var btnLogout = document.getElementById('btnLogout');
var btnImage = document.getElementById('file');
var profileImage = document.getElementById('profile-img');
var btnUploadProfile = document.getElementById('uploadProfile');

function init() {
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            userEmail = user.email;
            userName = userEmail.substring(0, userEmail.lastIndexOf("@"));
            $('#profile').find('#userName').text(userName);
            userRef = firebase.database().ref(userName);
            userGroupRef = firebase.database().ref(userName + '/group');
            userProfileRef = firebase.database().ref(userName + '/profile');

            var tmpName = '';
            var isRegistered = 0;

            registerRef.once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function (childshot) {
                        var childData = childshot.val();
                        if (childData.email != userEmail) {
                            tmpName = childData.email.substring(0, childData.email.lastIndexOf("@"));
                            if (childData.type == 0) {
                                $('<li class="contact friend" value="' + tmpName + '"><div class="wrap"><img alt="" /><div class="meta"><p class="name">' + childData.email + '</pre><p class="preview"></pre></div></div></li>').appendTo($('#contacts ul'));
                            }
                        } else {
                            isRegistered = 1;
                        }
                        first_count_person += 1
                    });
                    if (isRegistered == 0) {
                        registerRef.push({
                            email: userEmail,
                            type: 0
                        });
                        first_count_person += 1;
                    }

                    registerRef.on('child_added', function (data) {
                        second_count_person += 1;
                        if (second_count_person > first_count_person) {
                            var childData = data.val();
                            if (childData.email != userEmail) {
                                tmpName = childData.email.substring(0, childData.email.lastIndexOf("@"));
                                if (childData.type == 0) {
                                    $('<li class="contact friend" value="' + tmpName + '"><div class="wrap"><img alt=""/><div class="meta"><p class="name">' + childData.email + '</pre><p class="preview"></pre></div></div></li>').appendTo($('#contacts ul'));
                                }
                            }
                        }
                    });
                })
                .catch(e => console.log(e.message));

            userGroupRef.once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function (childshot) {
                        var childData = childshot.val();
                        $('<li class="contact group" value="' + childData.email + '"><div class="wrap"><img alt="" /><div class="meta"><p class="name">' + childData.email + '</pre><p class="preview"></pre></div></div></li>').appendTo($('#contacts ul'));
                        groupName = childData.email;
                        first_count_group += 1
                    });

                    userGroupRef.on('child_added', function (data) {
                        second_count_group += 1;
                        if (second_count_group > first_count_group) {
                            var childData = data.val();
                            $('<li class="contact group" value="' + childData.email + '"><div class="wrap"><img alt="" /><div class="meta"><p class="name">' + childData.email + '</pre><p class="preview"></pre></div></div></li>').appendTo($('#contacts ul'));
                            groupName = childData.email;
                        }
                    });
                })
                .catch(e => console.log(e.message));

            userProfileRef.on('child_added', function (data) {
                var childData = data.val();
                var url = childData.url;
                profileImage.src = url;
                profileImage.style.backgroundSize = "contain";
                profileImage.style.backgroundPosition = "center";
                profileImage.style.borderRadius = "50%";
                profileImage.style.width = "60px";
                profileImage.style.height = "60px";
            });

        } else {
            document.location.href = "./index.html";
        }
    });

    function readData() {
        $('.messages ul').empty();

        var first_count = 0;
        var second_count = 0;

        if (clickGroup) {
            groupRef.once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function (childshot) {
                        var childData = childshot.val();
                        if (childData.from == userEmail) {
                            if (childData.type == 0) {
                                $('<li class="replies" id="message"><pre>' + childData.data + '</pre></li>').appendTo($('.messages ul'));
                            } else {
                                $('<li class="replies" id="message"><img class="img pt-2" style="height: 20%; width: 20%;" src="' + childData.url + '"></li>').appendTo($('.messages ul'));
                            }
                        }
                        else {
                            if (childData.type == 0) {
                                $('<li class="sent" id="message"><pre>' + childData.data + '</pre></li>').appendTo($('.messages ul'));
                            } else {
                                $('<li class="sent" id="message"><img class="img pt-2" style="height: 20%; width: 20%;" src="' + childData.url + '"></li>').appendTo($('.messages ul'));
                            }
                        }
                        first_count += 1
                    });

                    groupRef.on('child_added', function (data) {
                        second_count += 1;
                        if (second_count > first_count) {
                            var childData = data.val();
                            if (childData.from == userEmail) {
                                if (childData.type == 0) {
                                    $('<li class="replies" id="message"><pre>' + childData.data + '</pre></li>').appendTo($('.messages ul'));
                                } else {
                                    $('<li class="replies" id="message"><img class="img pt-2" style="height: 20%; width: 20%;" src="' + childData.url + '"></li>').appendTo($('.messages ul'));
                                }
                            }
                            else {
                                if (childData.type == 0) {
                                    $('<li class="sent" id="message"><pre>' + childData.data + '</pre></li>').appendTo($('.messages ul'));
                                    var notifyConfig = {
                                        body: childData.data,
                                        tag: 'newArrival',
                                        renotify: true
                                    };
                                } else {
                                    $('<li class="sent" id="message"><img class="img pt-2" style="height: 20%; width: 20%;" src="' + childData.url + '"></li>').appendTo($('.messages ul'));
                                    var notifyConfig = {
                                        body: "Receive image",
                                        tag: 'newArrival',
                                        renotify: true
                                    };
                                }
                                if (Notification && Notification.permission === "granted") {
                                    var n = new Notification(groupName, notifyConfig);
                                }
                                else if (Notification && Notification.permission !== "denied") {
                                    Notification.requestPermission(function (status) {
                                        if (Notification.permission !== status) {
                                            Notification.permission = status;
                                        }
                                        if (status === "granted") {
                                            var n = new Notification(groupName, notifyConfig);
                                        }
                                    });
                                }
                            }
                        }
                    });
                })
                .catch(e => console.log(e.message));
        }
        else {
            userRef.once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function (childshot) {
                        var childData = childshot.val();
                        if (childData.from == userEmail && childData.to == friendEmail) {
                            if (childData.type == 0) {
                                $('<li class="replies" id="message"><pre>' + childData.data + '</pre></li>').appendTo($('.messages ul'));
                            } else {
                                $('<li class="replies" id="message"><img class="img pt-2" style="height: 20%; width: 20%;" src="' + childData.url + '"></li>').appendTo($('.messages ul'));
                            }
                        }
                        else if (childData.from == friendEmail) {
                            if (childData.type == 0) {
                                $('<li class="sent" id="message"><pre>' + childData.data + '</pre></li>').appendTo($('.messages ul'));
                            } else {
                                $('<li class="sent" id="message"><img class="img pt-2" style="height: 20%; width: 20%;" src="' + childData.url + '"></li>').appendTo($('.messages ul'));
                            }
                        }
                        first_count += 1
                    });

                    userRef.on('child_added', function (data) {
                        second_count += 1;
                        if (second_count > first_count) {
                            var childData = data.val();
                            if (childData.from == userEmail && childData.to == friendEmail) {
                                if (childData.type == 0) {
                                    $('<li class="replies" id="message"><pre>' + childData.data + '</pre></li>').appendTo($('.messages ul'));
                                } else {
                                    $('<li class="replies" id="message"><img class="img pt-2" style="height: 20%; width: 20%;" src="' + childData.url + '"></li>').appendTo($('.messages ul'));
                                }
                            }
                            else if (childData.from == friendEmail) {
                                if (childData.type == 0) {
                                    $('<li class="sent" id="message"><pre>' + childData.data + '</pre></li>').appendTo($('.messages ul'));
                                    var notifyConfig = {
                                        body: childData.data,
                                        tag: 'newArrival',
                                        renotify: true
                                    };
                                } else {
                                    $('<li class="sent" id="message"><img class="img pt-2" style="height: 20%; width: 20%;" src="' + childData.url + '"></li>').appendTo($('.messages ul'));
                                    var notifyConfig = {
                                        body: "Receive image",
                                        tag: 'newArrival',
                                        renotify: true
                                    };
                                }
                                if (Notification && Notification.permission === "granted") {
                                    var n = new Notification(friendName, notifyConfig);
                                }
                                else if (Notification && Notification.permission !== "denied") {
                                    Notification.requestPermission(function (status) {
                                        if (Notification.permission !== status) {
                                            Notification.permission = status;
                                        }
                                        if (status === "granted") {
                                            var n = new Notification(friendName, notifyConfig);
                                        }
                                    });
                                }

                            }
                        }
                    });
                })
                .catch(e => console.log(e.message));
        }
    }

    btnLogout.addEventListener("click", function () {
        firebase.auth().signOut().then(function () {
            alert("User sign out success!");
        }).catch(function (error) {
            alert("User sign out failed!");
        })
    }, false);

    btnUploadProfile.addEventListener('change', function (event) {
        event.preventDefault();
        var file = this.files[0];
        console.log(file);
        var storageRef = firebase.storage().ref(file.name);
        storageRef.put(file).then(function () {
            storageRef.getDownloadURL().then(function (url) {
                console.log(url);
                userProfileRef.push({
                    url: url
                });
            });
        });
    });

    btnImage.addEventListener('change', function (event) {
        event.preventDefault();
        var file = this.files[0];
        console.log(file);
        var storageRef = firebase.storage().ref(file.name);
        storageRef.put(file).then(function () {
            storageRef.getDownloadURL().then(function (url) {
                console.log(url);
                if (clickGroup) {
                    groupRef.push({
                        // Type 1 for image
                        type: 1,
                        from: userEmail,
                        to: friendEmail,
                        data: "",
                        url: url
                    });
                }
                else {
                    userRef.push({
                        // Type 1 for image
                        type: 1,
                        from: userEmail,
                        to: friendEmail,
                        data: "",
                        url: url
                    });
                    friendRef = firebase.database().ref(friendName);
                    friendRef.push({
                        // Type 1 for image
                        type: 1,
                        from: userEmail,
                        to: friendEmail,
                        data: "",
                        url: url
                    });
                }
                // readData(userEmail, userName, friendEmail);
            });
        });
    });
    post_txt.addEventListener('keydown', function (event) {
        if (event.which == 13) {
            post_btn.click();
        }
    });
    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            $('.contact.active .preview').html('<label>You: </label>' + post_txt.value);
            if (clickGroup) {
                groupRef.push({
                    // Type 0 for text
                    type: 0,
                    from: userEmail,
                    to: friendEmail,
                    data: post_txt.value,
                    url: ""
                });
            }
            else {
                userRef.push({
                    // Type 0 for text
                    type: 0,
                    from: userEmail,
                    to: friendEmail,
                    data: post_txt.value,
                    url: ""
                });
                friendRef = firebase.database().ref(friendName);
                friendRef.push({
                    // Type 0 for text
                    type: 0,
                    from: userEmail,
                    to: friendEmail,
                    data: post_txt.value,
                    url: ""
                });
            }
            post_txt.value = "";
            // readData(userEmail, userName, friendEmail);
        }
    });

    $(".select").on("click", "li", function (event) {
        event.preventDefault();
        $(".content").show();
        $("li").removeClass("active");
        $(this).toggleClass('active');
        var data = $(this).find('.name').text();
        friendEmail = data;
        friendName = friendEmail.substring(0, friendEmail.lastIndexOf("@"));
        if (friendName == "") {
            friendName = friendEmail;
            groupName = friendName;
            clickGroup = true;
            groupRef = firebase.database().ref(groupName);
            $("#memberDiv").show();
        } else {
            clickGroup = false;
            $("#memberDiv").hide();
        }
        $('.contact-profile').find('#contactName').text(friendName);
        readData();
    });
    addGroup.addEventListener('click', () => {
        addGroup.placeholder = "Enter group name";
    });
    addGroup.addEventListener('keydown', function (event) {
        if (event.which == 13) {
            $("li").removeClass("active");
            $('<li class="contact group" value="' + addGroup.value + '"><div class="wrap"><img alt="" /><div class="meta"><p class="name">' + addGroup.value + '</pre><p class="preview">Welcome!</pre></div></div></li>').appendTo($('#contacts ul'));
            $('.contact-profile').find('#contactName').text(addGroup.value);
            groupRef = firebase.database().ref(addGroup.value);
            var tmpRef = firebase.database().ref(userName + "/group");
            friendEmail = addGroup.value;
            friendName = addGroup.value;
            groupName = friendName;
            first_count_group += 1;
            tmpRef.push({
                email: addGroup.value
            });
            registerRef.push({
                email: groupName,
                type: 1
            });
            addGroup.blur();
            addGroup.value = "";
            addGroup.placeholder = "New group";
        }
    });

    addMember.addEventListener('keydown', function (event) {
        if (event.which == 13) {
            var isRegistered = 0;
            groupRef = firebase.database().ref(groupName);
            registerRef.once('child_added', function (data) {
                var childData = data.val();
                if (childData.email == addMember.value) {
                    isRegistered = 1;
                }
            }).then(() => {
                if (isRegistered == 0) {
                    create_alert("error", "Account have not register");
                } else {
                    create_alert("success", "add member success");
                    var tmpRef = firebase.database().ref(addMember.value.substring(0, addMember.value.lastIndexOf("@")) + "/group");
                    tmpRef.push({
                        email: groupName
                    });
                }
                addMember.value = "";
            });
        }
    });
}

function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    var str_html;
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    if (Notification && Notification.permission !== "granted") {
        Notification.requestPermission(function (status) {
            if (Notification.permission !== status) {
                Notification.permission = status;
            }
        });
    }

    init();
};

$(".messages").animate({ scrollTop: $(document).height() }, "fast");
